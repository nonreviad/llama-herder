use std::io::{Write, Read};

use anyhow::Result;
use os_pipe::{PipeWriter, PipeReader};
use serde::{Serialize, Deserialize};

pub fn send_to_pipe<T: Serialize + std::fmt::Debug>(pipe_writer: &mut PipeWriter, data: T) -> Result<()> {
    let data = serde_binary::to_vec(&data, serde_binary::binary_stream::Endian::Big)?;
    let sz = data.len() as u64;
    pipe_writer.write_all(&sz.to_be_bytes())?;
    pipe_writer.write_all(&data)?;
    Ok(())
}

pub fn get_from_pipe<T: for<'a> Deserialize<'a> + std::fmt::Debug>(pipe_reader: &mut PipeReader) -> Result<T> {
    let mut sz_bytes = [0u8; 8];
    pipe_reader.read_exact(&mut sz_bytes)?;
    let sz = u64::from_be_bytes(sz_bytes) as usize;
    let mut data = vec![0u8; sz];
    pipe_reader.read_exact(data.as_mut_slice())?;
    let ret = serde_binary::from_slice::<T>(data.as_slice(), serde_binary::binary_stream::Endian::Big).expect("Wat");
    Ok(ret)
}