#![allow(unused_mut)]
#![allow(dead_code)]
#![allow(unused_variables)]

use crate::{
    messages::{ModelOrWatchdogToParentMessage, ParentToModelOrWatchdogMessage, WebsocketToMainMessage, MainToWebsocketMessage},
    pipe_utils::{get_from_pipe, send_to_pipe},
    watchdog::watchdog_main_loop, websocket::websocket_main_loop,
};

use anyhow::Result;
use clap::Parser;
use fork::{fork, Fork};
use os_pipe::{pipe, PipeWriter};

mod messages;
mod model;
mod pipe_utils;
mod watchdog;
mod websocket;

#[derive(Debug, Clone, Parser)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[arg(value_name = "MODEL_DIR")]
    pub model_dir: String,
    #[arg(short, long, default_value = "6969", value_name = "PORT")]
    pub websocket_port: u16,
}

fn request_model_spinup(model_dir: &str, state_save_dir: &str, parent_writer: &mut PipeWriter) -> Result<()> {
    send_to_pipe(
        parent_writer,
        ParentToModelOrWatchdogMessage::SpinModel {
            model_dir: model_dir.to_owned(),
            save_session_dir: state_save_dir.to_owned(),
        },
    )?;
    Ok(())
}

fn main() -> Result<()> {
    let (mut model_reader, model_writer) = pipe()?;
    let (parent_reader, mut parent_writer) = pipe()?;
    let temp_dir = tempdir::TempDir::new("llama-herder")?;
    let state_save_dir = temp_dir.path().as_os_str().to_str().unwrap().to_owned();
    let pid: Fork = fork().expect("Cannot spawn child process :(");
    match pid {
        Fork::Parent(_) => {
            println!("Forked parent ^_^");
            drop(model_writer);
            drop(parent_reader);
        }
        Fork::Child => {
            drop(model_reader);
            drop(parent_writer);
            return watchdog_main_loop(model_writer, parent_reader);
        }
    }
    println!("Parent sending ping :pauseChamp:");
    send_to_pipe(&mut parent_writer, ParentToModelOrWatchdogMessage::Ping)?;
    let data = get_from_pipe::<ModelOrWatchdogToParentMessage>(&mut model_reader)?;
    match data {
        ModelOrWatchdogToParentMessage::Pong => println!("Parent got pong!"),
        _ => panic!(),
    }

    let args = Args::parse();

    request_model_spinup(&args.model_dir, &state_save_dir, &mut parent_writer)?;
    // Handle comms from websocket to main thread.
    let (tx, mut rx) = tokio::sync::mpsc::channel::<WebsocketToMainMessage>(1024);

    // Broadcast to all websockets, let them figure it out.
    let (mut b_tx, b_rx) = tokio::sync::broadcast::channel::<MainToWebsocketMessage>(1024);

    let runtime = tokio::runtime::Runtime::new()?;
    let handle = runtime.spawn(websocket_main_loop(args.websocket_port, tx, b_rx));
    loop {
        if let Some(data) = rx.blocking_recv() {
            match data {
                WebsocketToMainMessage::NewPrompt { session_id, data } => {
                    let message = ParentToModelOrWatchdogMessage::NewPrompt { session_id, data };
                    send_to_pipe(&mut parent_writer, message)?;
                    loop {
                        let data = get_from_pipe::<ModelOrWatchdogToParentMessage>(&mut model_reader)?;
                        match data {
                            ModelOrWatchdogToParentMessage::ModelDied => {
                                request_model_spinup(&args.model_dir, &state_save_dir, &mut parent_writer)?;
                                b_tx.send(MainToWebsocketMessage::ModelDied)?;
                                break;
                            },
                            ModelOrWatchdogToParentMessage::Pong => {
                                // Cool.
                            },
                            ModelOrWatchdogToParentMessage::Chunk { session_id, data } => {
                                b_tx.send(MainToWebsocketMessage::Chunk { session_id, data })?;
                            },
                            ModelOrWatchdogToParentMessage::Done { session_id, data } => {
                                b_tx.send(MainToWebsocketMessage::Done { session_id, data })?;
                                break;
                            },
                        }
                    }
                },
                WebsocketToMainMessage::NewSystemMessage { session_id, system_message } => {
                    let message = ParentToModelOrWatchdogMessage::NewSystemMessage { session_id, system_message };
                    send_to_pipe(&mut parent_writer, message)?;
                },
                WebsocketToMainMessage::NewSession { session_id } => {
                    let message = ParentToModelOrWatchdogMessage::NewSession { session_id };
                    send_to_pipe(&mut parent_writer, message)?;
                },
            }
        }        
    }

    // Ok(())
}
