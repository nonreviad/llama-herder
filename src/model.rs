use std::{path::PathBuf, fs::File, io::Read, io::{Write, stdout}};

use anyhow::Result;
use llama_cpp_rs::options::{ModelOptions, PredictOptions};
use os_pipe::{PipeReader, PipeWriter};
use serde::{Deserialize, Serialize};

use crate::{
    messages::{ModelOrWatchdogToParentMessage, ParentToModelOrWatchdogMessage},
    pipe_utils::{get_from_pipe, send_to_pipe},
};

pub fn model_main_loop(mut model_writer: PipeWriter, mut parent_reader: PipeReader) -> Result<()> {
    println!("New model, who dis");

    let data = get_from_pipe::<ParentToModelOrWatchdogMessage>(&mut parent_reader)?;
    match data {
        ParentToModelOrWatchdogMessage::Ping => {
            send_to_pipe(&mut model_writer, ModelOrWatchdogToParentMessage::Pong)?;
        }
        ParentToModelOrWatchdogMessage::SpinModel {
            model_dir,
            save_session_dir,
        } => {
            println!("Spinning up new model :nise:");
            let model_options = ModelOptions {
                ..Default::default()
            };
            let model = llama_cpp_rs::LLama::new(model_dir, &model_options).unwrap();
            println!("Model is up");
            inference_main_loop(model_writer, parent_reader, model, save_session_dir)?;
        }
        ParentToModelOrWatchdogMessage::NewSession { session_id } => todo!(),
        ParentToModelOrWatchdogMessage::NewPrompt { session_id, data } => todo!(),
        ParentToModelOrWatchdogMessage::NewSystemMessage { session_id, system_message } => todo!(),
    }
    Ok(())
}

fn inference_main_loop(mut model_writer: PipeWriter, mut parent_reader: PipeReader, mut model: llama_cpp_rs::LLama, mut save_sessions_dir: String) -> Result<()> {
    loop {
        let data = get_from_pipe::<ParentToModelOrWatchdogMessage>(&mut parent_reader)?;
        match data {
            ParentToModelOrWatchdogMessage::Ping => {
                send_to_pipe(&mut model_writer, ModelOrWatchdogToParentMessage::Pong)?;
            }
            ParentToModelOrWatchdogMessage::SpinModel {
                model_dir,
                save_session_dir: new_sessions_dir,
            } => {
                println!("Changing up model :nise:");
                let model_options = ModelOptions {
                    ..Default::default()
                };
                model = llama_cpp_rs::LLama::new(model_dir, &model_options).unwrap();
                save_sessions_dir = new_sessions_dir;
                println!("new model is up");
            }
            ParentToModelOrWatchdogMessage::NewSession { session_id } => {
                Session::new(session_id).to_file(&save_sessions_dir);
            },
            ParentToModelOrWatchdogMessage::NewPrompt { session_id, data } => {
                let mut session = Session::from_file(session_id, &save_sessions_dir);
                let writer_copy = model_writer.try_clone()?;
                let options = PredictOptions {
                    stop_prompts: vec!["</s>".to_owned()],
                    tokens: 0,
                    top_k: 90,
                    top_p: 0.86,
                    token_callback: Some(Box::new(move |token| {
                        print!("{token}");
                        let _ = stdout().flush();
                        let message = ModelOrWatchdogToParentMessage::Chunk { session_id, data: token.clone() };
                        let mut local_copy = writer_copy.try_clone().expect("Cannot make extra copy");
                        send_to_pipe(&mut local_copy, message).expect("Unable to send result thru pipe");
                        true
                    })),
                    ..Default::default()
                };
                let prompt = session.make_prompt(&data, &save_sessions_dir);
                // TODO: If it fails at this point, perhaps try to backoff on prompts and try again.
                let result = model.predict(prompt, options).expect("Failed to get output");
                let message = ModelOrWatchdogToParentMessage::Done { session_id, data: result };
                send_to_pipe(&mut model_writer, message)?;
            },
            ParentToModelOrWatchdogMessage::NewSystemMessage { session_id, system_message } => {
                let mut session = Session::from_file(session_id, &save_sessions_dir);
                session.system_message = system_message;
                session.to_file(&save_sessions_dir);
            },
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Session {
    session_id: u64,
    interactions: Vec<String>,
    system_message: String,
}

const DEFAULT_SYSTEM_MESSAGE: &'static str = r#"
You are an AI assistant whose purpose is to answer questions truthfully.
Your answering style needs to emulate that of an elderly Eastern European woman who is very kind, wise and into bodybuilding.
All your answers are short.
Your name is BuffBabushka.
"#;

impl Session {
    fn new(session_id: u64) -> Self {
        Self {
            session_id,
            interactions: vec![],
            system_message: DEFAULT_SYSTEM_MESSAGE.to_owned(),
        }
    }
    fn from_file(session_id: u64, save_dir: &str) -> Self {
        let path = PathBuf::from(format!("{save_dir}/{session_id}.json"));
        if let Ok(mut f) = File::open(path) {
            let mut data = String::new();
            f.read_to_string(&mut data).unwrap();
            serde_json::from_str(&data).expect("Unable to read session data")
        } else {
            let ret = Session::new(session_id);
            ret
        }
    }
    fn to_file(&self, save_dir: &str) {
        let path = PathBuf::from(format!("{save_dir}/{}.json", self.session_id));
        if let Ok(mut f) = File::create(path) {
            let data = serde_json::to_string(self).expect("Unable to serialize session");
            f.write_all(data.as_bytes()).expect("Unable to save session to file");
        } else {
            panic!("Unable to save session");
        }
    }
    fn make_prompt(&mut self, prompt: &str, save_dir: &str) -> String {
        self.interactions.push(prompt.to_owned());
        self.to_file(save_dir);
        let mut prompt = String::new();
        let system_message = self.system_message.trim();
        for (idx, interaction) in self.interactions.iter().enumerate() {
        let interaction = interaction.as_str().trim();
        if idx == 0 {
            prompt += &format!(
                "<s> [INST] <<SYS>>\n {system_message} \n<</SYS>>\n\n {interaction} [/INST] "
            );
        } else if idx % 2 == 0 {
            let interaction = if interaction.contains("</s>") {
                let (foo, _) = interaction.split_once("</s>").unwrap();
                foo.trim()
            } else {
                interaction
            };
            prompt += &format!(" </s> <s> [INST] {interaction} [/INST]");
        } else {
            prompt += interaction;
        }
        }
        prompt
    }
}