use crate::{pipe_utils::{get_from_pipe, send_to_pipe}, messages::ModelOrWatchdogToParentMessage, model::model_main_loop};
use crate::ParentToModelOrWatchdogMessage;

use anyhow::Result;
use fork::fork;
use libc::{waitpid, c_int};
use os_pipe::{PipeWriter, PipeReader};

fn sanity_check( model_writer:&mut PipeWriter, parent_reader: &mut PipeReader) -> Result<()> {
    let data = get_from_pipe::<ParentToModelOrWatchdogMessage>(parent_reader)?;
    match data {
        ParentToModelOrWatchdogMessage::Ping => println!("Child got ping!"),
        _ => panic!()
    }
    println!("Child sending pong");
    send_to_pipe(model_writer, ModelOrWatchdogToParentMessage::Pong)?;
    Ok(())
}

fn fork_me_daddy(model_writer: PipeWriter, parent_reader: PipeReader) -> i32 {
    match fork().expect("Could not form model process :(") {
        fork::Fork::Parent(pid) => {
            pid
        },
        fork::Fork::Child => {
            model_main_loop(model_writer, parent_reader).expect("Child dieded");
            panic!("Should never exit this");
        },
    }
}

pub fn watchdog_main_loop(mut model_writer: PipeWriter, mut parent_reader: PipeReader) -> Result<()> {
    sanity_check(&mut model_writer, &mut parent_reader)?;
    let mut child_pid: i32 = fork_me_daddy(model_writer.try_clone()?, parent_reader.try_clone()?);
    loop {
        let mut status: c_int = 0;
        unsafe{waitpid(child_pid, &mut status, 0)};
        println!("Model died. Respinning");
        send_to_pipe(&mut model_writer, ModelOrWatchdogToParentMessage::ModelDied)?;
        child_pid = fork_me_daddy(model_writer.try_clone()?, parent_reader.try_clone()?);
    }
}