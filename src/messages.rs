use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum ModelOrWatchdogToParentMessage {
    ModelDied,
    Pong,
    Chunk {
        session_id: u64,
        data: String,
    },
    Done {
        session_id: u64,
        data: String,
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum ParentToModelOrWatchdogMessage {
    Ping,
    SpinModel {
        model_dir: String,
        save_session_dir: String,
    },
    NewSession {
        session_id: u64
    },
    NewPrompt {
        session_id: u64,
        data: String
    },
    NewSystemMessage {
        session_id: u64,
        system_message: String,
    }
}

#[derive(Debug, Deserialize)]
pub enum WebsocketRequestType {
    NewSession,
    ChangeSystemMessage,
    Prompt
}

#[derive(Debug, Deserialize)]
pub struct WebsocketRequest {
    pub request_type: WebsocketRequestType,
    pub prompt: Option<String>
}

#[derive(Debug, Serialize)]
pub enum WebsocketResponseType {
    Chunk,
    Done
}

#[derive(Debug, Serialize)]
pub struct WebsocketResponse {
    pub response_type: WebsocketResponseType,
    pub prompt: Option<String>
}
// Messages sent from the main thread to the websocket threads
#[derive(Debug, Clone)]
pub enum MainToWebsocketMessage {
    Chunk {
        session_id: u64,
        data: String,
    },
    Done {
        session_id: u64,
        data: String,
    },
    ModelDied
}

// Messages sent from the websocket thread to the main thread
pub enum WebsocketToMainMessage {
    NewPrompt {
        session_id: u64,
        data: String,
    },
    NewSystemMessage {
        session_id: u64,
        system_message: String,
    },
    NewSession {
        session_id: u64
    }
}