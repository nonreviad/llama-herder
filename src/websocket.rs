use std::sync::{atomic::AtomicU64, Arc};

use anyhow::Result;
use futures_util::{SinkExt, StreamExt};
use tokio::net::{TcpListener, TcpStream};
use tokio_websockets::{Message, ServerBuilder, WebsocketStream};

use crate::messages::{MainToWebsocketMessage, WebsocketRequest, WebsocketToMainMessage, WebsocketResponse, WebsocketResponseType};

async fn websocket_conn_main_loop(
    mut stream: WebsocketStream<TcpStream>,
    session_id_generator: Arc<AtomicU64>,
    main_tx: tokio::sync::mpsc::Sender<WebsocketToMainMessage>,
    mut main_rx: tokio::sync::broadcast::Receiver<MainToWebsocketMessage>,
) -> Result<()> {
    let (mut ws_tx, mut rx) = stream.split();
    // Session ID is only needed server side :nise:
    let mut session_id = session_id_generator.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
    let mut ongoing_chunk = String::new();
    let mut has_ongoing_generation = false;
    loop {
        tokio::select! {
                    Some(Ok(data)) = rx.next() => {
                        if data.is_ping() {
                            ws_tx.send(Message::pong("")).await?;
                        } else if data.is_close() {
                            println!("Client disconnected");
                            break;
                        } else if data.is_text() {
                            let data = data.as_text().unwrap();
                            let data = serde_json::from_str::<WebsocketRequest>(data)?;
                            match data.request_type {
                                crate::messages::WebsocketRequestType::NewSession => {
                                    session_id =
                                        session_id_generator.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
                                    main_tx.send(WebsocketToMainMessage::NewSession { session_id }).await?;
                                }
                                crate::messages::WebsocketRequestType::ChangeSystemMessage => {
                                    main_tx.send(WebsocketToMainMessage::NewSystemMessage { session_id, system_message: data.prompt.unwrap() }).await?;
                                }
                                crate::messages::WebsocketRequestType::Prompt => {
                                    main_tx.send(WebsocketToMainMessage::NewPrompt { session_id, data: data.prompt.unwrap() }).await?;
                                    has_ongoing_generation = true;
                                }
                            }
                        }
                    }
                    Ok(data) = main_rx.recv() => {
                        match data {
                            MainToWebsocketMessage::Chunk { session_id: local_session_id, data } => {
                                if session_id != local_session_id {
                                    continue;
                                }
                                ongoing_chunk = format!("{ongoing_chunk}{data}");
                                let message = WebsocketResponse { response_type: WebsocketResponseType::Chunk , prompt: Some(ongoing_chunk.clone()) };
                                let message = serde_json::to_string(&message)?;
                                ws_tx.send(Message::text(message)).await?;
                            },
                            MainToWebsocketMessage::Done { session_id: local_session_id, data } => {
                                if session_id != local_session_id {
                                    continue;
                                }
                                let message = WebsocketResponse { response_type: WebsocketResponseType::Done , prompt: Some(ongoing_chunk.clone()) };
                                let message = serde_json::to_string(&message)?;
                                ws_tx.send(Message::text(message)).await?;
                                has_ongoing_generation = false;
                                ongoing_chunk = "".to_owned();
                            },
                            MainToWebsocketMessage::ModelDied => {
                                if !has_ongoing_generation {
                                    continue;
                                }
                                panic!("Unimplemented case");
                            },
                        }
                    }
                }
    }
    Ok(())
}

pub async fn websocket_main_loop(
    port: u16,
    main_tx: tokio::sync::mpsc::Sender<WebsocketToMainMessage>,
    main_rx: tokio::sync::broadcast::Receiver<MainToWebsocketMessage>,
) -> Result<()> {
    let addr = format!("0.0.0.0:{port}");
    println!("Listening on port {port}");
    let listener = TcpListener::bind(addr).await?;
    let session_id_generator = Arc::new(AtomicU64::new(69420));
    while let Ok((stream, _)) = listener.accept().await {
        let mut ws_stream = ServerBuilder::new().accept(stream).await?;
        tokio::spawn(websocket_conn_main_loop(
            ws_stream,
            session_id_generator.clone(),
            main_tx.clone(),
            main_rx.resubscribe(),
        ));
    }
    Ok(())
}
