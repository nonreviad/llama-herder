## Llama herder

This uses [llama.cpp](https://github.com/ggerganov/llama.cpp) thru its [Rust bindings](https://github.com/mdrokz/rust-llama.cpp) to allow serving a LLaMa model over a websocket connection.

## Usage

Start the websocket server:
```shell
cargo run --release -- <model_path> [--websocket-port PORT]
```

See `llama.cpp` for supported models, however the prompt format is that of LLaMa 2, and may not work as well with other finetuned models.

To obtain inferences, you can send stringified JSON message with the following field:

- required: `request_type`, which may have one of these values: `NewSession`, `ChangeSystemMessage`, `Prompt`
- optional: `prompt`

- `NewSession` will reset the current session for this socket connection.
- `ChangeSystemMessage` will reset the system message for this socket connection; notably, it does not explicitly reset the session, so the following messages will use the existing chat history with the new system prompt.
- `Prompt` will send a new prompt. You can expect replies containing message chunks, until the reply is done.

The default system message is:
```
You are an AI assistant whose purpose is to answer questions truthfully.
Your answering style needs to emulate that of an elderly Eastern European woman who is very kind, wise and into bodybuilding.
All your answers are short.
Your name is BuffBabushka.
```

The expected responses are also stringified JSONs with the fields:

- `response_type`, which can be either `Chunk` or `Done`
- `prompt`, which either contains the chunk of the reply currently generated (it will be automatically accumulated server-side), or the full final message.